# SimpleGram

SimpleGram adalah aplikasi media sosial sederhana berbasis foto dan deskripsi. Setelah mendaftar menjadi member, pengguna dapat mengupload foto atau gambar serta memberikan deskripsi pada foto tersebut. Selain itu, pengguna juga dapat melihat foto yang telah diupload oleh pengguna lain serta memberikan komentar pada pada postingan foto yang diinginkan.

### Fitur Tersedia

Beberapa fitur yang telah tersedia pada aplikasi ini adalah:

- Register untuk pengguna baru
- Login & Logout
- Posting foto baru
- Mengomentari foto yang ada
- Edit & Hapus pada postingan fotonya sendiri

### Fitur yang Belum Tersedia

Ada banyak sekali fitur yang belum tersedia pada aplikasi ini, yaitu:

- Reset password pengguna
- Edit profil pengguna
- Upload foto profil pengguna
- Like dan daftar like
- Bookmark Post
- Pesan private sederhana

Deskripsi lengkap dari fitur tersebut dapat dilihat pada halaman Issue.

### Deployment di Lokal

Simplegram ini dikembangkan menggunakan Framework Python Flask dengan struktur koding mengikuti pola HMVC.

Untuk instalasi aplikasi di PC / laptop pastikan sebelumnya sudah terinstall Python dan basis data MySQL, kemudian ikuti langkah berikut:

- Clone repositori ini di lokal
- Ubah koneksi basis data pada file `config.py`, sesuaikan dengan basis data yang terinstall
- Selanjutnya jalankan perintah di bawah ini pada terminal
    ```
    pip install -r requirements.txt
    python main.py
    ```